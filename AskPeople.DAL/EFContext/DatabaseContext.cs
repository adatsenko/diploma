﻿using AskPeople.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AskPeople.DAL.EFContext
{
    public class DatabaseContext: IdentityDbContext
    {
        public DbSet<User> UserSet { get; set; }
        public DbSet<Tag> TagSet { get; set; }
        public DbSet<UserAndTag> UserAndTagSet { get; set; }
        public DbSet<Interview> InterviewSet { get; set; }
        public DbSet<Voting> VotingSet { get; set; }
        public DbSet<VotingVariant> VotingVariantsSet { get; set; }
        public DbSet<InterviewParticipant> InterviewParticipantsSet { get; set; }
        public DbSet<VotingAnswer> VotingAnswerSet { get; set; }
        public DbSet<AlternativeEditor> AlternativeEditorsSet { get; set; }

        public DatabaseContext()
        {
            Database.EnsureCreated();
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserAndTag>()
                .HasKey(t => new { t.UserId, t.TagId });

            modelBuilder.Entity<UserAndTag>()
                .HasOne(u => u.User)
                .WithMany(s => s.Tags)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<UserAndTag>()
                .HasOne(t => t.Tag)
                .WithMany(s => s.Users)
                .HasForeignKey(t => t.TagId);

            modelBuilder.Entity<InterviewParticipant>()
                .HasKey(t => new { t.UserId, t.VotingId});

            modelBuilder.Entity<InterviewParticipant>()
                .HasOne(sc => sc.User)
                .WithMany(s => s.Invitations)
                .HasForeignKey(sc => sc.UserId);

            modelBuilder.Entity<InterviewParticipant>()
                .HasOne(sc => sc.Voting)
                .WithMany(c => c.Participants)
                .HasForeignKey(sc => sc.VotingId);

            modelBuilder.Entity<AlternativeEditor>()
                .HasKey(t => new { t.UserId, t.InterviewId });

            modelBuilder.Entity<AlternativeEditor>()
                .HasOne(sc => sc.User)
                .WithMany(s => s.EditInterviews)
                .HasForeignKey(sc => sc.UserId);

            modelBuilder.Entity<AlternativeEditor>()
                .HasOne(sc => sc.Interview)
                .WithMany(c => c.AlternativeEditors)
                .HasForeignKey(sc => sc.InterviewId);
        }
    }
}
