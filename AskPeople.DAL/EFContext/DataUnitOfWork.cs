﻿using AskPeople.DAL.Entities;
using AskPeople.DAL.Interfaces;
using System;
using AskPeople.DAL.Repositories;

namespace AskPeople.DAL.EFContext
{
    public class DataUnitOfWork: IDataUnitsOfWork
    {
        private bool _disposed = false;
        private readonly DatabaseContext _context;
        private UserRepository userRepo;
        private TagRepository tagRepo;
        private VotingRepository votingRepo;
        private VotingVariantRepository votingVariantRepo;
        private InterviewParticipantRepository interviewParticipantRepo;
        private VotingAnswerRepository votingAnswerRepo;
        private AlternativeEditorRepository alternativeEditorRepo;

        public DataUnitOfWork(DatabaseContext context)
        {
            _context = context;
        }

        public UserRepository UserRepo
        {
            get
            {
                if (userRepo == null)
                {
                    userRepo = new UserRepository(_context);
                }

                return userRepo;
            }
        }

        public TagRepository TagRepo
        {
            get
            {
                if (tagRepo == null)
                {
                    tagRepo = new TagRepository(_context);
                }

                return tagRepo;
            }
        }

        public VotingRepository VotingRepo
        {
            get
            {
                if (votingRepo == null)
                {
                    votingRepo = new VotingRepository(_context);
                }

                return votingRepo;
            }
        }

        public VotingVariantRepository VotingVariantRepo
        {
            get
            {
                if (votingVariantRepo == null)
                {
                    votingVariantRepo = new VotingVariantRepository(_context);
                }

                return votingVariantRepo;
            }
        }

        public InterviewParticipantRepository InterviewParticipantRepo
        {
            get
            {
                if (interviewParticipantRepo == null)
                {
                    interviewParticipantRepo = new InterviewParticipantRepository(_context);
                }

                return interviewParticipantRepo;
            }
        }

        public VotingAnswerRepository VotingAnswerRepo
        {
            get
            {
                if (votingAnswerRepo == null)
                {
                    votingAnswerRepo = new VotingAnswerRepository(_context);
                }

                return votingAnswerRepo;
            }
        }

        public AlternativeEditorRepository AlternativeEditorRepo
        {
            get
            {
                if (alternativeEditorRepo == null)
                {
                    alternativeEditorRepo = new AlternativeEditorRepository(_context);
                }

                return alternativeEditorRepo;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

    }
}
