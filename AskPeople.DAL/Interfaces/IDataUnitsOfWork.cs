﻿using AskPeople.DAL.Repositories;

namespace AskPeople.DAL.Interfaces
{
    public interface IDataUnitsOfWork
    {
        UserRepository UserRepo { get; }

        TagRepository TagRepo { get; }

        VotingRepository VotingRepo { get; }

        VotingVariantRepository VotingVariantRepo { get; }

        InterviewParticipantRepository InterviewParticipantRepo { get; }

        VotingAnswerRepository VotingAnswerRepo { get; }

        AlternativeEditorRepository AlternativeEditorRepo { get; }

        void Save();
    }
}
