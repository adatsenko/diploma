﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskPeople.DAL.Entities
{
    public class Voting: Interview
    {
        public int Count { get; set; }

        [Column("Type")]
        public string TypeString
        {
            get { return Type.ToString(); }
            private set { Type = (VotingType)Enum.Parse(typeof(VotingType), value, true); }
        }

        [NotMapped]
        public VotingType Type { get; set; }

        public List<VotingVariant> Variants { get; set; }
    }
}
