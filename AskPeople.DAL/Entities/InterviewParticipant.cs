﻿namespace AskPeople.DAL.Entities
{
    public class InterviewParticipant
    {
        public int Id { get; set; }
        public bool IsVoted { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public int VotingId { get; set; }
        public Voting Voting { get; set; }
    }
}
