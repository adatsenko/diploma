﻿using System.Collections.Generic;

namespace AskPeople.DAL.Entities
{
    public class VotingVariant
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public int VotingId { get; set; }
        public Voting Voting { get; set; }

        public List<VotingAnswer> Answers { get; set; }
    }
}
