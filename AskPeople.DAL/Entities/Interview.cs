﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskPeople.DAL.Entities
{
    public class Interview
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [Column("Access")]
        public string AccessString
        {
            get { return Access.ToString(); }
            private set { Access = (InterviewAccess) Enum.Parse(typeof(InterviewAccess), value, true); }
        }
        [NotMapped]
        public InterviewAccess Access { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public bool IsPublished { get; set; }
        public string SecretKey { get; set; }
        public string AlternativeOwners { get; set; }
        public string ParticipantsString { get; set; }
        public string TagsString { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public List<InterviewParticipant> Participants { get; set; }
        public List<AlternativeEditor> AlternativeEditors { get; set; }

        public Interview()
        {
            Participants = new List<InterviewParticipant>();
            AlternativeEditors = new List<AlternativeEditor>();
        }
    }
}
