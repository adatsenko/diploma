﻿using System.Collections.Generic;

namespace AskPeople.DAL.Entities
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<UserAndTag> Users { get; set; }

        public Tag()
        {
            Users = new List<UserAndTag>();
        }
    }
}
