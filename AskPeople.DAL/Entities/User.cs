﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace AskPeople.DAL.Entities
{
    public class User : IdentityUser
    {
        public string Nickname { get; set; }

        public virtual List<UserAndTag> Tags { get; set; }
        public List<Interview> Interviews { get; set; }
        public List<InterviewParticipant> Invitations { get; set; }
        public List<AlternativeEditor> EditInterviews { get; set; }

        public User()
        {
            Tags = new List<UserAndTag>();
            Interviews = new List<Interview>();
            Invitations = new List<InterviewParticipant>();
            EditInterviews = new List<AlternativeEditor>();
        }
    }
}
