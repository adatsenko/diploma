﻿namespace AskPeople.DAL.Entities
{
    public class UserAndTag
    {
        public string UserId { get; set; }
        public User User { get; set; }

        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }

    public class AlternativeEditor
    {
        public string UserId { get; set; }
        public User User { get; set; }

        public int InterviewId { get; set; }
        public Interview Interview { get; set; }
    }
}
