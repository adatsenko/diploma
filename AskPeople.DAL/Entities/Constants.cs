﻿namespace AskPeople.DAL.Entities
{
    public enum InterviewAccess
    {
        Public, 
        Protected,
        Private
    }

    public enum VotingType
    {
        Single,
        Multiply,
        Priority
    }
}
