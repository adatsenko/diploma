﻿namespace AskPeople.DAL.Entities
{
    public class VotingAnswer
    {
        public int Id { get; set; }
        public int Points { get; set; }

        public int VotingVariantId {get; set; }
        public VotingVariant Variant { get; set; }
    }
}
