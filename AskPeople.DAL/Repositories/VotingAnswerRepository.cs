﻿using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;

namespace AskPeople.DAL.Repositories
{
    public class VotingAnswerRepository: EntityRepository<VotingAnswer>
    {
        public VotingAnswerRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {

        }
    }
}
