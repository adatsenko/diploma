﻿using System.Collections.Generic;
using System.Linq;
using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace AskPeople.DAL.Repositories
{
    public class TagRepository: EntityRepository<Tag>
    {
        public TagRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {
            
        }

        public void InsertTagWithUser(List<Tag> tags, User user)
        {
            Tag tagToInsert;
            foreach (var tag in tags)
            {
                var isExist = dbSet.FirstOrDefault(t => t.Name == tag.Name);
                if (isExist != null)
                {
                    tagToInsert = isExist;
                }
                else
                {
                    Insert(tag);
                    tagToInsert = tag;
                }
                tagToInsert.Users.Add(new UserAndTag { TagId = tagToInsert.Id, UserId = user.Id });
            }
        }

        public void DeleteTagFromUser(List<Tag> tags, User user)
        {
            List<Tag> tagForDel = new List<Tag>();
            foreach (var tag in tags)
            {
                tagForDel.Add(Get(t => t.Name == tag.Name).First());
            }

            foreach (var tag in tagForDel)
            {
                var forDel = context.UserAndTagSet.First(e => e.UserId == user.Id && e.TagId == tag.Id);
                context.UserAndTagSet.Remove(forDel);
                var isLastUsing = context.UserAndTagSet.Where(e => e.TagId == tag.Id).ToList();
                if (isLastUsing.Count == 1)
                {
                    Delete(tag.Id);
                }
            }
        }

        public Tag GetTagsWithUsersByName(string name)
        {
            return dbSet.Include(t => t.Users).ThenInclude(e => e.User).FirstOrDefault(t => t.Name == name);
        }
    }
}
