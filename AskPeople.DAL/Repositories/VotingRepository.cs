﻿using System.Collections.Generic;
using System.Linq;
using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace AskPeople.DAL.Repositories
{
    public class VotingRepository: EntityRepository<Voting>
    {
        public VotingRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {

        }

        public List<Voting> GetUserVoting(string user)
        {
            var votings = dbSet.Include(v => v.User).Where(v => v.User.Nickname == user).ToList();
            return votings;
        }

        public List<Voting> GetVotingsSharedForEditing(string user)
        {
            var votings = dbSet.Include(u => u.User).Join(
                context.AlternativeEditorsSet.Include(e => e.User).Where(e => e.User.Nickname == user), v => v.Id,
                e => e.InterviewId,
                (v, e) => v).ToList();

            return votings;
        }

        public Voting GetWithVariants(int id)
        {
            var voting = dbSet.Include(v => v.Variants).ThenInclude(v => v.Answers).Include(v => v.User).First(v => v.Id == id);
            return voting;
        }

        public Voting GetWithVariantsAndEditors(int id)
        {
            var voting = dbSet.Include(v => v.Variants).ThenInclude(v => v.Answers).Include(v => v.User).
                Include(v => v.AlternativeEditors).ThenInclude(e => e.User).First(v => v.Id == id);
            return voting;
        }

        public void InsertVotingWithParticipants(Voting voting, List<User> participants)
        {
            foreach (var participant in participants)
            {
                voting.Participants.Add(new InterviewParticipant {IsVoted = false, UserId = participant.Id, VotingId = voting.Id});
            }
        }

        public List<Voting> GetUserInvitations(string name)
        {
            var invitations = dbSet.Include(v => v.User)
                .Join(context.InterviewParticipantsSet.Include(ip => ip.User).Where(ip => ip.User.Nickname == name), v => v.Id, ip => ip.VotingId,
                    (v, ip) => v).ToList();

            return invitations;
        }

        public Voting GetWithVariantAnswers(int votingId, string user)
        {
            var voting = dbSet.Include(v => v.User).Include(v => v.Variants).ThenInclude(v => v.Answers)
                .First(v => v.Id == votingId && v.User.UserName == user);

            return voting;
        }

        public void InsertVotingWithEditors(Voting voting, List<User> participants)
        {
            foreach (var participant in participants)
            {
                voting.AlternativeEditors.Add(new AlternativeEditor { UserId = participant.Id, InterviewId = voting.Id });
            }
        }
    }
}
