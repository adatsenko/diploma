﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;

namespace AskPeople.DAL.Repositories
{
    public class AlternativeEditorRepository: EntityRepository<AlternativeEditor>
    {
        public AlternativeEditorRepository(DatabaseContext context)
            : base(context)
        {
            
        }

        public List<AlternativeEditor> GetWithUser()
        {
            return dbSet.Include(e => e.User).ToList();
        }
    }
}
