﻿using System.Linq;
using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace AskPeople.DAL.Repositories
{
    public class InterviewParticipantRepository: EntityRepository<InterviewParticipant>
    {
        public InterviewParticipantRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {

        }

        public InterviewParticipant GetByIdAndUserWithVariants(int id, string name)
        {
            var result = dbSet.Include(ip => ip.User).Include(ip => ip.Voting).ThenInclude(v => v.Variants)
                .First(ip => (ip.User.Nickname == name) && (ip.Voting.Id == id));

            return result;
        }
    }
}
