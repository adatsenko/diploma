﻿using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace AskPeople.DAL.Repositories
{
    public class VotingVariantRepository: EntityRepository<VotingVariant>
    {
        public VotingVariantRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {

        }
    }
}
