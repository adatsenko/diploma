﻿using System.Collections.Generic;
using AskPeople.DAL.EFContext;
using AskPeople.DAL.Entities;
using System.Data.Entity;
using System.Linq;

namespace AskPeople.DAL.Repositories
{
    public class UserRepository: EntityRepository<User>
    {
        public UserRepository(DatabaseContext dbcontext)
            : base(dbcontext)
        {

        }

        public List<Tag> GetTagsForUser(string name)
        {
            List<Tag> tags = new List<Tag>();
            var user = dbSet.FirstOrDefault(u => u.Nickname == name);
            var tagsIds = context.UserAndTagSet.Where(e => e.UserId == user.Id).ToList();
            foreach (var id in tagsIds)
            {
                var tag = context.TagSet.Find(id.TagId);
                tags.Add(tag);
            }

            return tags;
        }

    }
}
