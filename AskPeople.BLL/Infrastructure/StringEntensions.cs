﻿using System;

namespace AskPeople.BLL.Infrastructure
{
    public static class StringEntensions
    {
        public static T ParseEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
