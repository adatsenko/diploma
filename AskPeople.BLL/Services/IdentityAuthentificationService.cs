﻿using AskPeople.BLL.DTO;
using AskPeople.BLL.Interfaces;
using AskPeople.DAL.Entities;
using AskPeople.DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AskPeople.BLL.Services
{
    public class IdentityAuthentificationService: IAuthenticationService
    {
        private const string NicknameAlreadyExist = "This nickname have already registered!";
        private const string InvalidLoginPassword = "Incorrect nickname or password";

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IDataUnitsOfWork _dataProvider;

        public IDataUnitsOfWork DataProvider => _dataProvider;
       
        public IdentityAuthentificationService(UserManager<User> userManager, SignInManager<User> signInManager,
            IDataUnitsOfWork dataProvider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dataProvider = dataProvider;
        }

        public async Task<IList<string>> RegisterUser(RegisterModelDTO model)
        {
            List<string> errors = new List<string>();
            User user = new User() { Email = model.Email, Nickname = model.Nickname, UserName = model.Nickname };

            var isUserExist = _dataProvider.UserRepo.Get(u => u.Nickname == user.Nickname);
            if (isUserExist.Count() != 0)
            {
                errors.Add(NicknameAlreadyExist);
                return errors;
            }

            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                if (model.Tags.Count != 0)
                {
                    _dataProvider.TagRepo.InsertTagWithUser(model.Tags, user);
                    await SignInUser(user);
                    _dataProvider.Save();
                }
                return null;
            }
            
            foreach (var error in result.Errors)
            {
                errors.Add(error.Description);
            }

            return errors;
        }

        public async Task SignInUser(User user)
        {
            await _signInManager.SignInAsync(user, false);
        }

        public async Task<IList<string>> PasswordSignInUser(LoginModelDTO model)
        {
            List<string> errors = new List<string>();
            var result =
                await _signInManager.PasswordSignInAsync(model.Nickname, model.Password, model.RememberMe, false);

            if (!result.Succeeded)
            {
                errors.Add(InvalidLoginPassword);
            }

            return errors;
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<List<string>> UpdateUserProfile(EditProfileModelDTO model)
        {
            List<string> errors = new List<string>();

            if (!model.Nickname.Equals(model.OldNickname))
            {
                var isUserExist = _dataProvider.UserRepo.Get(u => u.Nickname == model.Nickname);
                if (isUserExist.Count() != 0)
                {
                    errors.Add(NicknameAlreadyExist);
                    return errors;
                }
            }

            User user = await _userManager.FindByIdAsync(model.Id);
            if (user != null)
            {
                user.Email = model.Email;
                user.Nickname = model.Nickname;
                user.UserName = model.Nickname;
            }

            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                List<Tag> tagsForAdding = GetTagException(model.Tags, model.OldTags);
                _dataProvider.TagRepo.InsertTagWithUser(tagsForAdding, user);

                List<Tag> tagsForDeleting = GetTagException(model.OldTags, model.Tags);
                _dataProvider.TagRepo.DeleteTagFromUser(tagsForDeleting, user);
                _dataProvider.Save();
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    errors.Add(error.Description);
                }    
            }

            return errors;
        }

        private List<Tag> GetTagException(List<string> tags, List<string> oldTags)
        {
            List<Tag> result = new List<Tag>();
            if (tags == null || tags.Count == 0)
            {
                return result;
            }

            var tagsForAdd = tags.Except(oldTags);
            foreach (var tag in tagsForAdd)
            {
                result.Add(new Tag{Name = tag});
            }

            return result;
        }
    }
}
