﻿using System;
using AskPeople.BLL.DTO;
using AskPeople.BLL.Interfaces;
using AskPeople.DAL.Entities;
using AskPeople.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace AskPeople.BLL.Services
{
    public class InterviewService: IInterviewService
    {
        private readonly IDataUnitsOfWork _dataProvider;

        public IDataUnitsOfWork DataProvider => _dataProvider;

        public InterviewService(IDataUnitsOfWork dataProvider)
        {
           _dataProvider = dataProvider;
        }

        public void CreateVoting(VotingModelDTO model, string userName)
        {
            var user = _dataProvider.UserRepo.Get(u => u.UserName == userName).First();

            var voting = new Voting()
            {
                Name = model.Name,
                Description = model.Description,
                StartTime = model.StartTime,
                FinishTime = model.FinishTime,
                IsPublished = model.IsPublished,
                Access = model.Access,
                SecretKey = model.SecretKey ?? "",
                Count = model.Type == VotingType.Single ? 1 : model.Count,
                Type = model.Type,
                User = user,
                ParticipantsString = model.ParticipantsString,
                TagsString = model.TagsString
            };

            _dataProvider.VotingRepo.Insert(voting);

            List<VotingVariant> variants = new List<VotingVariant>();
            foreach (var variant in model.Variants)
            {
                variants.Add(new VotingVariant {Value = variant, Voting = voting});
            }

            _dataProvider.VotingVariantRepo.Insert(variants);

            _dataProvider.Save();

            if (voting.IsPublished)
            {
                PublishVoting(voting, model.Participants, model.Tags);
            }

            if (model.AlternativeEditors.Count != 0)
            {
                SetAlternativeEditors(voting, model.AlternativeEditors);
            }
        }

        public Voting GetVotingById(int id)
        {
            return _dataProvider.VotingRepo.GetWithVariantsAndEditors(id);
        }

        public void UpdateVoting(VotingModelDTO model)
        {
            var voting = _dataProvider.VotingRepo.GetWithVariantsAndEditors(model.Id);
            
            var oldVariants = VariantsToStringList(voting.Variants);
            var oldAltenatives = AlternativeEditorsToStringList(voting.AlternativeEditors);
            voting.Name = model.Name;
            voting.Description = model.Description;
            voting.Access = model.Access;
            voting.StartTime = model.StartTime;
            voting.FinishTime = model.FinishTime;
            voting.SecretKey = model.SecretKey ?? "";
            voting.IsPublished = model.IsPublished;
            voting.Type = model.Type;
            voting.Count = model.Type == VotingType.Single ? 1 : model.Count;
            voting.ParticipantsString = model.ParticipantsString;
            voting.TagsString = model.TagsString;

            var variantsForAdding = GetVariantsException(model.Variants, oldVariants);
            foreach (var v in variantsForAdding)
            {
                v.Voting = voting;
            }
            _dataProvider.VotingVariantRepo.Insert(variantsForAdding);
            
           
            var variantsForDeleting = GetVariantsException(oldVariants, model.Variants);
            foreach (var var in variantsForDeleting)
            {
                var delete = voting.Variants.First(v => v.Value == var.Value);
                _dataProvider.VotingVariantRepo.Delete(delete);
            }

            if (model.AlternativeEditors != null && model.AlternativeEditors.Count != 0)
            {
                var editorsForAdding = GetStringListException(model.AlternativeEditors, oldAltenatives);
                SetAlternativeEditors(voting, editorsForAdding);

                var editorsForDeleting = GetStringListException(oldAltenatives, model.AlternativeEditors);
                DeleteEditors(editorsForDeleting);
            }

            _dataProvider.Save();

            if (voting.IsPublished)
            {
                PublishVoting(voting, model.Participants, model.Tags);
            }
        }

        public void DeleteVoting(int id)
        {
            var voting = _dataProvider.VotingRepo.GetWithVariants(id);
            foreach (var var in voting.Variants)
            {
                _dataProvider.VotingVariantRepo.Delete(var);
                foreach (var answer in var.Answers)
                {
                    _dataProvider.VotingAnswerRepo.Delete(answer);
                }
            }

            _dataProvider.VotingRepo.Delete(voting);
            _dataProvider.Save();
        }

        public List<UserInterviewDTO> GetUserVotings(string user)
        {
            List<UserInterviewDTO> votingsDTO = new List<UserInterviewDTO>();
            var votings = _dataProvider.VotingRepo.GetUserVoting(user);
            foreach (var voting in votings)
            {
                votingsDTO.Add(new UserInterviewDTO(){Id = voting.Id, Name = voting.Name, Description = voting.Description, IsPublished = voting.IsPublished});
            }

            return votingsDTO;
        }

        public List<UserInterviewDTO> GetVotingsSharedForEditing(string user)
        {
            List<UserInterviewDTO> votingsDTO = new List<UserInterviewDTO>();
            var votings = _dataProvider.VotingRepo.GetVotingsSharedForEditing(user);
            foreach (var voting in votings)
            {
                votingsDTO.Add(new UserInterviewDTO() { Id = voting.Id, Name = voting.Name, Description = voting.Description, IsPublished = voting.IsPublished, AllowEditing = true, Owner = voting.User.Nickname});
            }

            return votingsDTO;
        }

        public void PublishVoting(Voting voting, List<string> participants, List<string> tags)
        {

            List<User> users = new List<User>();
            if (tags != null && tags.Count != 0)
            {
                foreach (var t in tags)
                {
                    var tag = _dataProvider.TagRepo.GetTagsWithUsersByName(t);
                    if (tag != null)
                    {
                        foreach (var u in tag.Users)
                        {
                            var user = _dataProvider.UserRepo.Get(e => e.Nickname == u.User.Nickname).First();
                            if (user != null)
                            {
                                users.Add(user);
                            }
                        }
                    }
                }
            }

            foreach (var p in participants)
            {
                var user = _dataProvider.UserRepo.Get(u => u.Nickname == p).First();
                if (user != null)
                {
                    users.Add(user);
                }
            }

            _dataProvider.VotingRepo.InsertVotingWithParticipants(voting, users.Distinct().ToList());
            _dataProvider.Save();
        }

        public List<InterviewInvitationDTO> GetUserInvitations(string name)
        {
            var votings = _dataProvider.VotingRepo.GetUserInvitations(name);
            List<InterviewInvitationDTO> invitations = new List<InterviewInvitationDTO>();
            foreach (var voting in votings)
            {
                var interviewParticipant =
                    _dataProvider.InterviewParticipantRepo.GetByIdAndUserWithVariants(voting.Id, name);
                invitations.Add(new InterviewInvitationDTO{Id = voting.Id, Description = voting.Description, Owner = voting.User.Nickname, Title = voting.Name, IsVoted = interviewParticipant.IsVoted});
            }

            return invitations;
        }

        public InterviewParticipant GetVotingByIdForVote(int id, string name)
        {
            var voting = _dataProvider.InterviewParticipantRepo.GetByIdAndUserWithVariants(id, name);
            if (voting == null || voting.IsVoted)
            {
                return null;
            }

            return voting;
        }

        public void MakeVote(MakeVoteModelDTO model, string name)
        {
            if (model.Type == VotingType.Priority)
            {
                MakePriorityVote(model);
            }

            if (model.Type == VotingType.Single)
            {
                MakeSingleVote(model);
            }

            if (model.Type == VotingType.Multiply)
            {
                MakeMultipleVote(model);
            }
           
            SetVotingAsVotedForUser(model.Id, name);
        }

        public VotingResultModelDTO GetVotingResult(int votingId, string name)
        {
            var voting = _dataProvider.VotingRepo.GetWithVariantAnswers(votingId, name);
            if (voting != null)
            {
                VotingResultModelDTO dto = new VotingResultModelDTO
                {
                    VotingId = voting.Id,
                    Count = voting.Count == 0 ? 1 : voting.Count,
                    Description = voting.Description,
                    Name = voting.Name,
                    Type = voting.Type,
                    Owner = voting.User.Nickname
                };

                foreach (var variant in voting.Variants)
                {
                    var points = variant.Answers.Sum(a => a.Points);
                    dto.VariantResults.Add(new VariantResultDTO{Points = points, Variant = variant.Value});
                }

                return dto;
            }

            return null;
        }

        public void SetAlternativeEditors(Voting voting, List<string> alternatives)
        {
            List<User> users = new List<User>();
            foreach (var name in alternatives)
            {
                var user = _dataProvider.UserRepo.Get(u => u.Nickname == name).First();
                if (user != null)
                {
                    users.Add(user);
                }
            }

            _dataProvider.VotingRepo.InsertVotingWithEditors(voting, users);
            _dataProvider.Save();
        }





        private void MakeSingleVote(MakeVoteModelDTO model)
        {
            List<string> answers = new List<string>();
            foreach (var variant in model.Variants)
            {
                if (variant.Points != 0)
                {
                    answers.Add(variant.Value);
                }
            }

            if (answers.Count == 1)
            {
                var votingVariant =
                    _dataProvider.VotingVariantRepo.Get(v => v.Value == answers[0] && v.VotingId == model.Id);

                VotingAnswer answer = new VotingAnswer { Points = 1, Variant = votingVariant.First() };
                _dataProvider.VotingAnswerRepo.Insert(answer);
                _dataProvider.Save();
            }
        }

        private void MakePriorityVote(MakeVoteModelDTO model)
        {
            List<VariantsStructureDTO> answers = new List<VariantsStructureDTO>();
            foreach (var variant in model.Variants)
            {
                if (variant.Points != 0)
                {
                    answers.Add(variant);
                }
            }

            foreach (var a in answers)
            {
                var variant = _dataProvider.VotingVariantRepo.Get(v => v.Value == a.Value && v.VotingId == model.Id).First();
                VotingAnswer answer = new VotingAnswer { Points = model.Count + 1 - a.Points, Variant = variant };
                _dataProvider.VotingAnswerRepo.Insert(answer);
            }

            _dataProvider.Save();
        }

        private void MakeMultipleVote(MakeVoteModelDTO model)
        {
            List<string> answers = new List<string>();
            foreach (var variant in model.Variants)
            {
                if (variant.Points != 0)
                {
                    answers.Add(variant.Value);
                }
            }

            if (answers.Count <= model.Count)
            {
                List<VotingVariant> votingVariants = new List<VotingVariant>();
                foreach (var a in answers)
                {
                    var variant = _dataProvider.VotingVariantRepo.Get(v => v.Value == a && v.VotingId == model.Id).First();
                    VotingAnswer answer = new VotingAnswer{Points = 1, Variant = variant};
                    _dataProvider.VotingAnswerRepo.Insert(answer);
                }

                _dataProvider.Save();
            }
        }

        private void SetVotingAsVotedForUser(int id, string name)
        {
            var voting = _dataProvider.InterviewParticipantRepo.GetByIdAndUserWithVariants(id, name);
            if (voting != null)
            {
                voting.IsVoted = true;
                _dataProvider.InterviewParticipantRepo.Update(voting);
                _dataProvider.Save();
            }
        }

        private List<string> VariantsToStringList(List<VotingVariant> variants)
        {
            List<string> result = new List<string>();
            foreach (var variant in variants)
            {
                result.Add(variant.Value);
            }

            return result;
        }

        private List<string> AlternativeEditorsToStringList(List<AlternativeEditor> users)
        {
            List<string> result = new List<string>();
            foreach (var user in users)
            {
                result.Add(user.User.Nickname);
            }

            return result;
        }

        private List<VotingVariant> GetVariantsException(List<string> var, List<string> oldVar)
        {
            List<VotingVariant> result = new List<VotingVariant>();
            var values = GetStringListException(var, oldVar);
            foreach (var v in values)
            {
                result.Add(new VotingVariant() { Value = v });
            }

            return result;
        }

        private List<string> GetStringListException(List<string> newValues, List<string> oldValues)
        {
            List<string> result = new List<string>();
            if (newValues == null || newValues.Count == 0)
            {
                return result;
            }

            var valuesForAdd = newValues.Except(oldValues);
            foreach (var value in valuesForAdd)
            {
                result.Add(value);
            }

            return result;
        }

        private void DeleteEditors(List<string> editors)
        {
            foreach (var editor in editors)
            {
                var alternative = _dataProvider.AlternativeEditorRepo.GetWithUser().FirstOrDefault(e => e.User.Nickname == editor);
                _dataProvider.AlternativeEditorRepo.Delete(alternative);
            }
        }
    }
}
