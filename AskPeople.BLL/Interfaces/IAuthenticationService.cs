﻿using AskPeople.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using AskPeople.BLL.DTO;
using AskPeople.DAL.Interfaces;

namespace AskPeople.BLL.Interfaces
{
    public interface IAuthenticationService
    {
        IDataUnitsOfWork DataProvider { get; }

        Task<IList<string>> RegisterUser(RegisterModelDTO model);

        Task SignInUser(User user);

        Task<IList<string>> PasswordSignInUser(LoginModelDTO model);

        Task SignOut();

        Task<List<string>> UpdateUserProfile(EditProfileModelDTO model);
    }
}
