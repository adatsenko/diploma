﻿using System.Collections.Generic;
using AskPeople.BLL.DTO;
using AskPeople.DAL.Entities;

namespace AskPeople.BLL.Interfaces
{
    public interface IInterviewService
    {
        void CreateVoting(VotingModelDTO model, string userName);

        List<UserInterviewDTO> GetUserVotings(string user);

        List<UserInterviewDTO> GetVotingsSharedForEditing(string user);

        Voting GetVotingById(int id);

        void UpdateVoting(VotingModelDTO model);

        void DeleteVoting(int id);

        void PublishVoting(Voting voting, List<string> participants, List<string> tags);

        List<InterviewInvitationDTO> GetUserInvitations(string name);

        InterviewParticipant GetVotingByIdForVote(int id, string name);

        void MakeVote(MakeVoteModelDTO model, string name);

        VotingResultModelDTO GetVotingResult(int votingId, string name);
    }
}
