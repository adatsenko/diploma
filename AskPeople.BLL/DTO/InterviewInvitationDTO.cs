﻿namespace AskPeople.BLL.DTO
{
    public class InterviewInvitationDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
        public bool IsVoted { get; set; }
    }
}
