﻿using System;
using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.BLL.DTO
{
    public class VotingModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public InterviewAccess Access { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public bool IsPublished { get; set; }
        public string SecretKey { get; set; }
        public List<string> AlternativeEditors { get; set; }
        public List<string> Participants { get; set; }
        public string ParticipantsString { get; set; }
        public List<string> Tags { get; set; }
        public string TagsString { get; set; }
        public int Count { get; set; }
        public List<string> Variants { get; set; }
        public VotingType Type { get; set; }
    }
}
