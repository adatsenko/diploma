﻿namespace AskPeople.BLL.DTO
{
    public class UserInterviewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPublished { get; set; }
        public bool AllowEditing { get; set; }
        public string Owner { get; set; }
    }
}
