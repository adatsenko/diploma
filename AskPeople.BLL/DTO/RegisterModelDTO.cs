﻿using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.BLL.DTO
{
    public class RegisterModelDTO
    {
        public string Email { get; set; }

        public string Nickname { get; set; }

        public string Password { get; set; }

        public List<Tag> Tags { get; set; }

        public RegisterModelDTO()
        {
            Tags = new List<Tag>();
        }
    }
}
