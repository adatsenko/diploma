﻿using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.BLL.DTO
{
    public class MakeVoteModelDTO
    {
        public int Id { get; set; }
        public VotingType Type { get; set; }
        public int Count { get; set; }
        public List<VariantsStructureDTO> Variants { get; set; }

        public MakeVoteModelDTO()
        {
            Variants = new List<VariantsStructureDTO>();
        }
    }

    public class VariantsStructureDTO
    {
        public string Value { get; set; }
        public int Points { get; set; }
    }
}
