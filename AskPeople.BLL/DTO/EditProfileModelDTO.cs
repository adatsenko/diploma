﻿using System.Collections.Generic;

namespace AskPeople.BLL.DTO
{
    public class EditProfileModelDTO
    {
        public string Id { get; set; }
        public string Nickname { get; set; }
        public string OldNickname { get; set; }
        public string Email { get; set; }
        public string OldEmail { get; set; }
        public List<string> Tags { get; set; }
        public List<string> OldTags { get; set; }

        public EditProfileModelDTO()
        {
            Tags = new List<string>();
            OldTags = new List<string>();
        }
    }
}
