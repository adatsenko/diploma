﻿using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.BLL.DTO
{
    public class VotingResultModelDTO
    {
        public int VotingId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public VotingType Type { get; set; }
        public int Count { get; set; }
        public List<VariantResultDTO> VariantResults { get; set; }
        public string Owner { get; set; }

        public VotingResultModelDTO()
        {
            VariantResults = new List<VariantResultDTO>();
        }
    }

    public class VariantResultDTO
    {
        public string Variant { get; set; }

        public int Points { get; set; }
    }
}
