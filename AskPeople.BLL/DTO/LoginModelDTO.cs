﻿namespace AskPeople.BLL.DTO
{
    public class LoginModelDTO
    {
        public string Nickname { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
