﻿using System.Collections.Generic;
using AskPeople.BLL.DTO;
using AskPeople.BLL.Interfaces;

namespace AskPeople.Web.Models
{
    public class HomeModel
    {
        private IInterviewService _interviewService;

        public HomeModel(IInterviewService interviewService)
        {
            _interviewService = interviewService;
        }

        public List<UserInterviewDTO> GetUserVotings(string user)
        {
            return _interviewService.GetUserVotings(user);
        }

        public List<InterviewInvitationDTO> GetInvitations(string user)
        {
            return _interviewService.GetUserInvitations(user);
        }

        public List<UserInterviewDTO> GetSharedForEditing(string user)
        {
            return _interviewService.GetVotingsSharedForEditing(user);
        }
    }
}
