﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AskPeople.BLL.DTO;
using AskPeople.BLL.Interfaces;
using AskPeople.DAL.Entities;
using AskPeople.Web.ViewModels;
using Microsoft.CodeAnalysis.CSharp;

namespace AskPeople.Web.Models
{
    public class AuthentificationModel
    {
        private const string TagPattern = @"^#[\w_]+$";

        private IAuthenticationService _authenticationService;

        public AuthentificationModel(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public async Task<IList<string>> ValidateAndRegister(RegistrationModel model)
        {
            RegisterModelDTO registerDTO = new RegisterModelDTO()
            {
                Email = model.Email,
                Nickname = model.Nickname,
                Password = model.Password
            };

            if (!string.IsNullOrEmpty(model.Tags))
            {
                foreach (var tag in GetTagsFromString(model.Tags))
                {
                    registerDTO.Tags.Add(new Tag() {Name = tag});
                }
            }

            return await _authenticationService.RegisterUser(registerDTO);
        }

        public async Task<IList<string>> SignInUser(LoginViewModel model)
        {
           LoginModelDTO dto = new LoginModelDTO
            {
                Nickname = model.Nickname,
                Password = model.Password,
                RememberMe = model.RememberMe
            };
            
            return await _authenticationService.PasswordSignInUser(dto);
        }

        public async Task SignOutUser()
        {
            await _authenticationService.SignOut();
        }

        public User GetUserByName(string name)
        {
            return _authenticationService.DataProvider.UserRepo.Get(u => u.Nickname == name).FirstOrDefault();
        }

        public List<string> GetTagsForUser(string name)
        {
            List<string> tagsNames = new List<string>();
            var tags = _authenticationService.DataProvider.UserRepo.GetTagsForUser(name);
            foreach (var tag in tags)
            {
                tagsNames.Add(tag.Name);
            }

            return tagsNames;
        }

        public async Task<List<string>> UpdateUserProfile(EditProfileViewModel model)
        {
            EditProfileModelDTO editDTO =
                new EditProfileModelDTO() {Id = model.Id, Nickname = model.Nickname, Email = model.Email, OldEmail = model.OldEmail, OldNickname = model.OldNickname};

            if(!string.IsNullOrEmpty(model.Tags))
            foreach (var tag in GetTagsFromString(model.Tags))
            {
                editDTO.Tags.Add(tag);
            }

            if(!string.IsNullOrEmpty(model.OldTags))
            foreach (var oldtag in GetTagsFromString(model.OldTags))
            {
                editDTO.OldTags.Add(oldtag);
            }

            return await _authenticationService.UpdateUserProfile(editDTO);
        }

        private List<string> GetTagsFromString(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            List<string> tags = new List<string>();

            var tagsFromString = str.Split(' ');
            foreach (var tag in tagsFromString)
            {
                if (Regex.IsMatch(tag, TagPattern))
                {
                    tags.Add(tag);
                }
            }

            return tags;
        }
    }
}
