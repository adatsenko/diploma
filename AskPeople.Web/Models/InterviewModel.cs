﻿using AskPeople.BLL.DTO;
using AskPeople.BLL.Interfaces;
using AskPeople.DAL.Entities;
using AskPeople.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AskPeople.Web.Models
{
    public class InterviewModel
    {
        private IInterviewService _interviewService;

        public InterviewModel(IInterviewService interviewService)
        {
            _interviewService = interviewService;
        }

        public void CreateVoting(VotingViewModel model, string userName)
        {
            VotingModelDTO modelDTO = new VotingModelDTO
            {
                Name = model.Name,
                Description = model.Description,
                Access = model.Access,
                SecretKey = model.Password,
                StartTime = model.StartTime,
                FinishTime = model.FinishTime,
                Participants = GetNamesFromString(model.Participants),
                AlternativeEditors = GetNamesFromString(model.AlternativeEditors),
                Type = model.Type,
                IsPublished = model.IsPublished,
                Count = model.AnswerCount,
                Variants = model.Variants,
                ParticipantsString = model.Participants,
                Tags = GetNamesFromString(model.Tags),
                TagsString = model.Tags
            };

            _interviewService.CreateVoting(modelDTO, userName);
        }

        public VotingViewModel GetVotingById(int id)
        {
            var voting = _interviewService.GetVotingById(id);
            if (voting == null)
            {
                return null;
            }

            VotingViewModel model = new VotingViewModel()
            {
                Name = voting.Name,
                Id = voting.Id,
                Description = voting.Description,
                Access = voting.Access,
                StartTime = voting.StartTime,
                FinishTime = voting.FinishTime,
                Password = voting.SecretKey,
                Type = voting.Type,
                AnswerCount = voting.Count,
                Variants = VariantsToStringList(voting.Variants),
                User = voting.User,
                Participants = voting.ParticipantsString,
                AlternativeEditors = AlternativeEditorsToString(voting.AlternativeEditors),
                Tags = voting.TagsString
            };

            return model;
        }

        public void EditVoting(VotingViewModel model)
        {
            VotingModelDTO modelDTO = new VotingModelDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Access = model.Access,
                SecretKey = model.Password,
                StartTime = model.StartTime,
                FinishTime = model.FinishTime,
                Participants = GetNamesFromString(model.Participants),
                Type = model.Type,
                IsPublished = model.IsPublished,
                Count = model.AnswerCount,
                Variants = model.Variants,
                ParticipantsString = model.Participants,
                Tags = GetNamesFromString(model.Tags),
                TagsString = model.Tags
            };

            if (model.AlternativeEditors != null)
            {
                modelDTO.AlternativeEditors = GetNamesFromString(model.AlternativeEditors);
            }

            _interviewService.UpdateVoting(modelDTO);
        }

        public void DeleteVoting(int id)
        {
            _interviewService.DeleteVoting(id);
        }

        public MakeVoteViewModel GetVotingForVote(int id, string name)
        {
            var voting = _interviewService.GetVotingByIdForVote(id, name);
            if (voting == null)
            {
                return null;
            }

            MakeVoteViewModel model = new MakeVoteViewModel
            {
                Id = voting.Voting.Id,
                Type = voting.Voting.Type,
                Count = voting.Voting.Count,
            };

            foreach (var variant in voting.Voting.Variants)
            {
                model.Variants.Add(new VariantsStructure { Value = variant.Value, Points = 0 });
            }

            return model;
        }

        public void MakeVote(MakeVoteViewModel model, string name)
        {
            MakeVoteModelDTO dto = new MakeVoteModelDTO
            {
                Id = model.Id,
                Count = model.Count,
                Type = model.Type,
            };

            foreach (var variant in model.Variants)
            {
                dto.Variants.Add(new VariantsStructureDTO {Value = variant.Value, Points = variant.Points});
            }

            _interviewService.MakeVote(dto, name);
        }

        public VotingResultViewModel GetVotingResults(int id, string name)
        {
            var dto = _interviewService.GetVotingResult(id, name);
            if (dto != null)
            {
                VotingResultViewModel model = new VotingResultViewModel
                {
                    VotingId = dto.VotingId,
                    Count = dto.Count,
                    Description = dto.Description,
                    Name = dto.Name,
                    Type = dto.Type,
                    Owner = dto.Owner
                };

                foreach (var variantResult in dto.VariantResults)
                {
                    model.VariantResults.Add(new VariantResult{Points = variantResult.Points, Variant = variantResult.Variant});
                }

                return model;
            }

            return null;
        }

        public bool AllowedEditing(int voting, string user)
        {
            var votings = _interviewService.GetVotingsSharedForEditing(user).Where(v => v.Id == voting);
            return votings != null;
        }







        private List<string> GetNamesFromString(string participants)
        {
            List<string> names = new List<string>();
            if (!string.IsNullOrEmpty(participants))
            {
                foreach (var name in participants.Trim().Split(' '))
                {
                    if (name.Trim() != "")
                    {
                        names.Add(name.Trim());
                    }
                }
            }

            return names;
        }

        private List<string> VariantsToStringList(List<VotingVariant> variants)
        {
            List<string> result = new List<string>();
            foreach (var variant in variants)
            {
                result.Add(variant.Value);
            }

            return result;
        }

        private string AlternativeEditorsToString(List<AlternativeEditor> editors)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var editor in editors)
            {
                sb.Append(editor.User.Nickname + " ");
            }

            return sb.ToString().Trim();
        }
    }
}
