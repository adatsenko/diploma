﻿using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.Web.ViewModels
{
    public class MakeVoteViewModel
    {
        public int Id { get; set; }
        public VotingType Type { get; set; }
        public int Count { get; set; }
        public List<VariantsStructure> Variants { get; set; }

        public MakeVoteViewModel()
        {
            Variants = new List<VariantsStructure>();
        }
    }

    public class VariantsStructure
    {
        public string Value { get; set; }
        public int Points { get; set; }
    }
}
