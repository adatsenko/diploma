﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AskPeople.DAL.Entities;

namespace AskPeople.Web.ViewModels
{
    public class VotingViewModel: InterviewViewModel
    {
        [Required]
        public VotingType Type { get; set; }


        public int AnswerCount { get; set; }

        [Required]
        public List<string> Variants { get; set; }

        public VotingViewModel()
        {
            Variants = new List<string>();
        }
    }
}
