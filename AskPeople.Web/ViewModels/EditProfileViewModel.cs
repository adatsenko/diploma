﻿using System.ComponentModel.DataAnnotations;

namespace AskPeople.Web.ViewModels
{
    public class EditProfileViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Nickname { get; set; }

        public string Tags { get; set; }

        public string OldTags { get; set; }
        public string OldNickname { get; set; }
        public string OldEmail { get; set; }
    }
}
