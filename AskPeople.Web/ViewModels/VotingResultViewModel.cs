﻿using System.Collections.Generic;
using AskPeople.DAL.Entities;

namespace AskPeople.Web.ViewModels
{
    public class VotingResultViewModel
    {
        public int VotingId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public VotingType Type { get; set; }
        public int Count { get; set; }
        public List<VariantResult> VariantResults { get; set; }
        public string Owner { get; set; }

        public VotingResultViewModel()
        {
            VariantResults = new List<VariantResult>();
        }
    }

    public class VariantResult
    {
        public string Variant { get; set; }

        public int Points { get; set; }
    }
}
