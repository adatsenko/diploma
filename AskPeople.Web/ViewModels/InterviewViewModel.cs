﻿using System;
using System.ComponentModel.DataAnnotations;
using AskPeople.DAL.Entities;

namespace AskPeople.Web.ViewModels
{
    public class InterviewViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public InterviewAccess Access { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime FinishTime { get; set; }

        public string Password { get; set; }

        public bool IsPublished { get; set; }

        [Required]
        public string Participants { get; set; }

        public string AlternativeEditors { get; set; }

        public string Tags { get; set; }

        public User User { get; set; }
    }
}
