﻿using System.Collections.Generic;
using AskPeople.BLL.DTO;

namespace AskPeople.Web.ViewModels
{
    public class HomeViewModel
    {
        public List<UserInterviewDTO> UserInterviews { get; set; }
        public List<InterviewInvitationDTO> Invitation { get; set; }
        public List<UserInterviewDTO> SharedForEditing { get; set; }

        public HomeViewModel()
        {
            UserInterviews = new List<UserInterviewDTO>();
            Invitation = new List<InterviewInvitationDTO>();
            SharedForEditing = new List<UserInterviewDTO>();
        }
    }
}
