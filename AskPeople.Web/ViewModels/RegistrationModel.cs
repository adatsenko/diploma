﻿using System.ComponentModel.DataAnnotations;

namespace AskPeople.Web.ViewModels
{
    public class RegistrationModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Nickname { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords is not equal!")]
        [DataType(DataType.Password)]
        public string Password2 { get; set; }

        public string Tags { get; set; }
    }
}
