﻿using System.ComponentModel.DataAnnotations;

namespace AskPeople.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        public string Nickname { get; set; }

        [Required]
        public string Password { get; set; }
        
        [Display(Name = "Remember?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
