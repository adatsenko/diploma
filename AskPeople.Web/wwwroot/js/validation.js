﻿//Validation of the different forms

function validateLoginView() {
    event.preventDefault();
    var loginForm = document.getElementById('loginForm');
    var validNick = validateNickname();
    var validPass = validatePassword();
    if (validNick && validPass)
        loginForm.submit();

    return;
}

function validateRegisterView() {
    event.preventDefault();
    var registerForm = document.getElementById('registerForm');
    var validNick = validateNickname();
    var validPass = validatePassword();
    var arePassEq = arePasswordsEqual();
    var validEmail = validateEmail();
    if (validNick && validPass && validEmail && arePassEq)
        registerForm.submit();

    return;
}

function validateProfileView() {
    event.preventDefault();
    var profileForm = document.getElementById('profileForm');
    var validNick = validateNickname();
    var validEmail = validateEmail();
    if (validNick && validEmail)
        profileForm.submit();

    return;
}

function validateVotingEditionView() {
    event.preventDefault();
    var votingForm = document.getElementById('voting-form');
    var isNameValid = validateVotingName();
    var isDescrValid = validateVotingDescription();
    var isCountValid = validateVotingAnswerCount();
    var isPassValid = validateVotingPassword();
    var isVariantsValid = validateVotingVariants();

    if (isNameValid && isDescrValid && isCountValid && isPassValid && isVariantsValid) {
        votingForm.submit();
    }

    return;
}



//Additional functions for validation

function validateNickname() {
    var nicknameElement = document.getElementById('nickname');
    var validationElement = document.getElementById('nickname-validation');
    validationElement.innerText = '';
    var data = nicknameElement.value;

    if (isEmptyString(data)) {
        validationElement.innerText = 'The nickname field cannot be empty!';
        return false;
    }

    if (data.length <= 4) {
        validationElement.innerText = 'The nickname must contain more than 5 symbols!';
        return false;
    }

    return true;
}

function validatePassword() {
    var passwordElement = document.getElementById('password');
    var validationElement = document.getElementById('password-validation');
    var password = passwordElement.value;
    validationElement.innerText = '';

    if (isEmptyString(password)) {
        validationElement.innerText = 'The password field cannot be empty!';
        return false;
    }

    if (password.length <= 4) {
        validationElement.innerText = 'The password must contain more than 5 symbols!';
        return false;
    }

    return true;
}

function arePasswordsEqual() {
    var passwordElement = document.getElementById('password');
    var password2Element = document.getElementById('password2');
    var validationElement = document.getElementById('password-validation');
    var password = passwordElement.value;
    var password2 = password2Element.value;
    validationElement.innerText = '';

    if (password != password2) {
        validationElement.innerText = 'The passwords are not equal!';
        return false;
    }

    return true;
}

function validateEmail() {
    var emailElement = document.getElementById('email');
    var validationElement = document.getElementById('email-validation');
    var email = emailElement.value;
    validationElement.innerText = '';

    if (isEmptyString(email)) {
        validationElement.innerText = 'The email field cannot be empty!';
        return false;
    }

    return true;
}

function validateVotingName() {
    var nameElement = document.getElementById('voting-name');
    var validationElement = document.getElementById('voting-name-validation');
    validationElement.innerText = '';
    var data = nameElement.value;

    if (isEmptyString(data)) {
        validationElement.innerText = 'The name field cannot be empty!';
        return false;
    }

    if (data.length <= 4) {
        validationElement.innerText = 'The name must contain more than 5 symbols!';
        return false;
    }

    return true;
}

function validateVotingDescription() {
    var descrElement = document.getElementById('voting-description');
    var validationElement = document.getElementById('voting-description-validation');
    validationElement.innerText = '';
    var data = descrElement.value;

    if (isEmptyString(data)) {
        validationElement.innerText = 'The description field cannot be empty!';
        return false;
    }

    if (data.length <= 9) {
        validationElement.innerText = 'The description must contain more than 10 symbols!';
        return false;
    }

    return true;
}

function validateVotingAnswerCount() {
    var countInput = document.getElementById('voting-count-block');
    if (countInput.style.display === "none")
        return true;
    var countElement = document.getElementById('voting-answ-count');
    var validationElement = document.getElementById('voting-answ-count-validation');
    validationElement.innerText = '';
    var data = +countElement.value;

    var variants = document.getElementsByClassName('voting-var');

    if (data > variants.length) {
        validationElement.innerText = 'The count of answers cannot be more than count of variants!';
        return false;
    }

    return true;
}

function validateVotingPassword() {
    var passwordElement = document.getElementById('voting-password-block');
    if (passwordElement.style.display == "none")
        return true;
    var validationElement = document.getElementById('voting-password-validation');
    var password = passwordElement.value;
    validationElement.innerText = '';

    if (isEmptyString(password)) {
        validationElement.innerText = 'The password field cannot be empty!';
        return false;
    }

    if (password.length <= 4) {
        validationElement.innerText = 'The password must contain more than 5 symbols!';
        return false;
    }

    return true;
}

function validateVotingVariants() {
    var variants = document.getElementsByClassName('voting-var');
    for (var i = 0; i < variants.length; i++) {
        var parent = variants[i].parentNode;
        var validationWarning = parent.lastElementChild;
        if (isEmptyString(variants[i].value)) {
            validationWarning.innerText = 'This field cannot be an empty!';
            return false;
        } else {
            validationWarning.innerText = '';
        }
    }

    return true;
}

function isEmptyString(str) {
    if (str == null || str.trim() == '')
        return true;

    return false;
}
