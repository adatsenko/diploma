﻿var variantsElCollection = [];
var count = 0;
var type = "";

createVoting();

function validateFormSubmition() {
    event.preventDefault();
    var voteForm = document.getElementById('vote-form');
    var inputs = document.getElementsByClassName("helper-text");
    var warning = document.getElementById("val-warning");
    warning.innerText = '';

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value != 0) {
            voteForm.submit();
            return;
        }
    }

    warning.innerText = "You should change one variant as minimum!";
}

function createVoting() {
    variantsElCollection = document.getElementsByClassName("voting-variant");
    var countEl = document.getElementById("voting-count");
    var typeEl = document.getElementById("voting-type");

    count = countEl.value;
    type = typeEl.value;

    if (type != "Priority") {
        createSingleMultiplyVoting();
    }
    
}

function createSingleMultiplyVoting() {
    var helpers = document.getElementsByClassName("helper-text");

    for (var i = 0; i < helpers.length; i++) {
        helpers[i].style.display = "none";
    }

    for (var i = 0; i < variantsElCollection.length; i++) {
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.name = i;
        checkbox.onclick = type == "Single" ? validateSingleVoting : validateMultiplyVoting;
        variantsElCollection[i].appendChild(checkbox);
    }
}

function validateSingleVoting(event) {
    var inputs = document.getElementsByClassName("helper-text");
    var warning = document.getElementById("val-warning");
    warning.innerText = '';

    if (event.target.checked == true) {
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].value == 1) {
                warning.innerText = "You've already changed one variant!";
                event.preventDefault();
                return;
            }
        }
        inputs[event.target.name].value = 1;
    } else {
        inputs[event.target.name].value = 0;
    }
}

function validateMultiplyVoting(event) {
    var inputs = document.getElementsByClassName("helper-text");
    var warning = document.getElementById("val-warning");
    warning.innerText = '';
    var counter = 0;

    if (event.target.checked == true) {
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].value == 1) {
                counter++;
                if (++counter > count) {
                    warning.innerText = "You've already changed max count of variants!";
                    event.preventDefault();
                    return;
                }
            }
        }
        inputs[event.target.name].value = 1;
    } else {
        inputs[event.target.name].value = 0;
    }
}

function validatePriorityVoting(event) {
    var value = event.target.value;
    var inputs = document.getElementsByClassName("helper-text");
    var warning = document.getElementById("val-warning");
    warning.innerText = '';
    var counter = 0;
    var valueCounter = 0;

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value != 0) {
            counter++;
            if (counter > count) {
                warning.innerText = "You've already changed max count of variants!";
                return;
            }
        }
        if (inputs[i].value == value) {
            valueCounter++;
            if (++valueCounter > 2) {
                warning.innerText = "You've already entered the same priority value!";
                return;
            }
        }
    }

    var repeats = 0;
    for (var j = 0; j < inputs.length; j++) {
        repeats = 0;
        for (var k = 0; k < inputs.length; k++) {
            if (inputs[j].value == inputs[k].value && (inputs[j].value != "" && inputs[k].value)) {
                repeats++;
            }
        }
        if (repeats > 1) {
            warning.innerText = "You've already entered the same priority value!";
            return;
        }
    }
}