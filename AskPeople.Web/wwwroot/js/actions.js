﻿function onVotingAccessChanged() {
    var accessElement = document.getElementById('voting-access');
    var data = accessElement.value;
    showPasswordFieldOnVoting(data);
}

function onVotingTypeChanged() {
    var typeElement = document.getElementById('voting-type');
    var data = typeElement.value;
    showCountOnVoting(data);
}

function onAddClickOnVoting() {
    var div = document.createElement("div");
    div.className += "voting-variant";
    div.innerHTML =
        "<input type='text' class='form-control voting-var' id='Variants' name='Variants' placeholder='Enter variant'>" +
        "<button class='btn btn-danger del-voting-var' type='button' onclick='onDeleteClickOnVoting(event)'> Remove</button>" +
        "<span class='validate-warning'></span>";
    var container = document.getElementById("voting-variants-list");
    container.appendChild(div);
}

function onDeleteClickOnVoting(event) {
    var variants = document.getElementsByClassName('voting-var');
    if (variants.length == 1)
        return;
    var delbtn = event.currentTarget;
    var block = delbtn.parentNode;
    var container = document.getElementById("voting-variants-list");
    container.removeChild(block);
}




function showCountOnVoting(value) {
    var countElement = document.getElementById('voting-count-block');

    if (value != null && value !== "Single") {
        countElement.style.display = "block";
    } else {
        countElement.style.display = "none";
    }
}

function showPasswordFieldOnVoting(value) {
    var passElement = document.getElementById('voting-password-block');

    if (value != null && value === "Private") {
       passElement.style.display = "block";
    } else {
        passElement.style.display = "none";
    }
}