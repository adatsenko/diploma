﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AskPeople.Web.Migrations
{
    public partial class RevertInterviewParticipants : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_InterviewId",
                table: "InterviewParticipantsSet");

            migrationBuilder.RenameColumn(
                name: "InterviewId",
                table: "InterviewParticipantsSet",
                newName: "VotingId");

            migrationBuilder.RenameIndex(
                name: "IX_InterviewParticipantsSet_InterviewId",
                table: "InterviewParticipantsSet",
                newName: "IX_InterviewParticipantsSet_VotingId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_VotingId",
                table: "InterviewParticipantsSet",
                column: "VotingId",
                principalTable: "InterviewSet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_VotingId",
                table: "InterviewParticipantsSet");

            migrationBuilder.RenameColumn(
                name: "VotingId",
                table: "InterviewParticipantsSet",
                newName: "InterviewId");

            migrationBuilder.RenameIndex(
                name: "IX_InterviewParticipantsSet_VotingId",
                table: "InterviewParticipantsSet",
                newName: "IX_InterviewParticipantsSet_InterviewId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_InterviewId",
                table: "InterviewParticipantsSet",
                column: "InterviewId",
                principalTable: "InterviewSet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
