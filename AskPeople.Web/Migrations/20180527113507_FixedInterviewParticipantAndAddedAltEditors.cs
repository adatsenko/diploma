﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AskPeople.Web.Migrations
{
    public partial class FixedInterviewParticipantAndAddedAltEditors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_VotingId",
                table: "InterviewParticipantsSet");

            migrationBuilder.RenameColumn(
                name: "VotingId",
                table: "InterviewParticipantsSet",
                newName: "InterviewId");

            migrationBuilder.RenameIndex(
                name: "IX_InterviewParticipantsSet_VotingId",
                table: "InterviewParticipantsSet",
                newName: "IX_InterviewParticipantsSet_InterviewId");

            migrationBuilder.CreateTable(
                name: "AlternativeEditorsSet",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    InterviewId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AlternativeEditorsSet", x => new { x.UserId, x.InterviewId });
                    table.ForeignKey(
                        name: "FK_AlternativeEditorsSet_InterviewSet_InterviewId",
                        column: x => x.InterviewId,
                        principalTable: "InterviewSet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AlternativeEditorsSet_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AlternativeEditorsSet_InterviewId",
                table: "AlternativeEditorsSet",
                column: "InterviewId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_InterviewId",
                table: "InterviewParticipantsSet",
                column: "InterviewId",
                principalTable: "InterviewSet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_InterviewId",
                table: "InterviewParticipantsSet");

            migrationBuilder.DropTable(
                name: "AlternativeEditorsSet");

            migrationBuilder.RenameColumn(
                name: "InterviewId",
                table: "InterviewParticipantsSet",
                newName: "VotingId");

            migrationBuilder.RenameIndex(
                name: "IX_InterviewParticipantsSet_InterviewId",
                table: "InterviewParticipantsSet",
                newName: "IX_InterviewParticipantsSet_VotingId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterviewParticipantsSet_InterviewSet_VotingId",
                table: "InterviewParticipantsSet",
                column: "VotingId",
                principalTable: "InterviewSet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
