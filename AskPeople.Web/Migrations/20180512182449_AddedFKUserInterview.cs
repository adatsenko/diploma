﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AskPeople.Web.Migrations
{
    public partial class AddedFKUserInterview : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "InterviewSet",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_InterviewSet_UserId",
                table: "InterviewSet",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterviewSet_AspNetUsers_UserId",
                table: "InterviewSet",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterviewSet_AspNetUsers_UserId",
                table: "InterviewSet");

            migrationBuilder.DropIndex(
                name: "IX_InterviewSet_UserId",
                table: "InterviewSet");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "InterviewSet");
        }
    }
}
