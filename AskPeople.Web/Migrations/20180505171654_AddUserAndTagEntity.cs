﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AskPeople.Web.Migrations
{
    public partial class AddUserAndTagEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAndTag_Tag_TagId",
                table: "UserAndTag");

            migrationBuilder.DropForeignKey(
                name: "FK_UserAndTag_AspNetUsers_UserId",
                table: "UserAndTag");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserAndTag",
                table: "UserAndTag");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tag",
                table: "Tag");

            migrationBuilder.RenameTable(
                name: "UserAndTag",
                newName: "UserAndTagSet");

            migrationBuilder.RenameTable(
                name: "Tag",
                newName: "TagSet");

            migrationBuilder.RenameIndex(
                name: "IX_UserAndTag_TagId",
                table: "UserAndTagSet",
                newName: "IX_UserAndTagSet_TagId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserAndTagSet",
                table: "UserAndTagSet",
                columns: new[] { "UserId", "TagId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_TagSet",
                table: "TagSet",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAndTagSet_TagSet_TagId",
                table: "UserAndTagSet",
                column: "TagId",
                principalTable: "TagSet",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserAndTagSet_AspNetUsers_UserId",
                table: "UserAndTagSet",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserAndTagSet_TagSet_TagId",
                table: "UserAndTagSet");

            migrationBuilder.DropForeignKey(
                name: "FK_UserAndTagSet_AspNetUsers_UserId",
                table: "UserAndTagSet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserAndTagSet",
                table: "UserAndTagSet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TagSet",
                table: "TagSet");

            migrationBuilder.RenameTable(
                name: "UserAndTagSet",
                newName: "UserAndTag");

            migrationBuilder.RenameTable(
                name: "TagSet",
                newName: "Tag");

            migrationBuilder.RenameIndex(
                name: "IX_UserAndTagSet_TagId",
                table: "UserAndTag",
                newName: "IX_UserAndTag_TagId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserAndTag",
                table: "UserAndTag",
                columns: new[] { "UserId", "TagId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tag",
                table: "Tag",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserAndTag_Tag_TagId",
                table: "UserAndTag",
                column: "TagId",
                principalTable: "Tag",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserAndTag_AspNetUsers_UserId",
                table: "UserAndTag",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
