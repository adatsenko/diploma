﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AskPeople.Web.Migrations
{
    public partial class AddedInterviewParticipant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InterviewParticipantsSet",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    VotingId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    IsVoted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterviewParticipantsSet", x => new { x.UserId, x.VotingId });
                    table.ForeignKey(
                        name: "FK_InterviewParticipantsSet_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InterviewParticipantsSet_InterviewSet_VotingId",
                        column: x => x.VotingId,
                        principalTable: "InterviewSet",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InterviewParticipantsSet_VotingId",
                table: "InterviewParticipantsSet",
                column: "VotingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InterviewParticipantsSet");
        }
    }
}
