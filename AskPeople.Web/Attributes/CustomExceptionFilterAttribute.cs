﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AskPeople.Web.Attributes
{
    public class CustomExceptionFilterAttribute: Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (!context.ExceptionHandled)
            {
                context.Result = new RedirectResult("/Error/Index");
                context.ExceptionHandled = true;
            }
        }
    }
}
