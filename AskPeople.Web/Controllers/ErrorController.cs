﻿using Microsoft.AspNetCore.Mvc;


namespace AskPeople.Web.Controllers
{
    public class ErrorController : Controller
    {
        const string CommonError = "Error. You try to do incorrect actions or somethings goes wrong!";


        public IActionResult Index()
        {
            return View("Index", CommonError);
        }

        public IActionResult StatusError(int id)
        {
            return View("StatusError", id.ToString());
        }
    }
}
