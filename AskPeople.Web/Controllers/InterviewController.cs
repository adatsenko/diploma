﻿using System;
using AskPeople.BLL.Interfaces;
using AskPeople.Web.Attributes;
using AskPeople.Web.Models;
using AskPeople.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AskPeople.Web.Controllers
{
    [CustomExceptionFilter]
    [Authentification]
    public class InterviewController : Controller
    {
        private readonly InterviewModel _interviewModel;

        public InterviewController(IInterviewService interviewService)
        {
            _interviewModel = new InterviewModel(interviewService);
        }

        [HttpGet]
        public IActionResult CreateVoting()
        {
            return View("CreateVoting");
        }

        [HttpPost]
        public IActionResult CreateVoting(VotingViewModel model)
        {
            if (ModelState.IsValid || ModelState.ErrorCount == 1)
            {
                _interviewModel.CreateVoting(model, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

            return View("CreateVoting", model);
        }
        
        [HttpGet]
        public IActionResult EditVoting(int id)
        {
            var voting = _interviewModel.GetVotingById(id);
            if (voting == null)
            {
                return NotFound();
            }
            if (voting.User.Nickname != User.Identity.Name && !_interviewModel.AllowedEditing(id, User.Identity.Name))
            {
                throw new Exception();
            }
            
            return View("EditVoting", voting);
        }

        [HttpPost]
        public IActionResult EditVoting(VotingViewModel model)
        {
            if (ModelState.IsValid || ModelState.ErrorCount == 1)
            {
                _interviewModel.EditVoting(model);
                return RedirectToAction("Index", "Home");
            }

            return View("EditVoting", model);
        }

        [HttpPost]
        public IActionResult DeleteVoting(int id)
        {
            _interviewModel.DeleteVoting(id);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Vote(int id)
        {
            var voting = _interviewModel.GetVotingForVote(id, User.Identity.Name);
            if (voting == null)
            {
                throw new Exception();
            }

            return View("MakeVote", voting);
        }

        [HttpPost]
        public IActionResult Vote(MakeVoteViewModel model)
        {
            _interviewModel.MakeVote(model, User.Identity.Name);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult ViewResults(int id)
        {
            var model = _interviewModel.GetVotingResults(id, User.Identity.Name);
            if (model == null)
            {
                throw new Exception();
            }
            if (model.Owner != User.Identity.Name && !_interviewModel.AllowedEditing(id, User.Identity.Name))
            {
                throw new Exception();
            }

            return View("VotingResults", model);
        }

    }
}
