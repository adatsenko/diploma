﻿using AskPeople.BLL.Interfaces;
using AskPeople.Web.Attributes;
using AskPeople.Web.Models;
using AskPeople.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AskPeople.Web.Controllers
{
    [Authentification]
    public class HomeController : Controller
    {
        private readonly HomeModel _homeModel;

        public HomeController(IInterviewService interviewService)
        {
            _homeModel = new HomeModel(interviewService);
        }

        public IActionResult Index()
        {
            string userName = User.Identity.Name;
            HomeViewModel model = new HomeViewModel();
            model.UserInterviews = _homeModel.GetUserVotings(userName);
            model.Invitation = _homeModel.GetInvitations(userName);
            model.SharedForEditing = _homeModel.GetSharedForEditing(userName);
            return View("Index", model);
        }
    }
}
