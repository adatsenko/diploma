﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AskPeople.BLL.Interfaces;
using AskPeople.DAL.Entities;
using AskPeople.Web.Attributes;
using AskPeople.Web.Models;
using AskPeople.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AskPeople.Web.Controllers
{
    [Authentification]
    public class ProfileController : Controller
    {
        private readonly AuthentificationModel _authentificationModel;

        public ProfileController(IAuthenticationService authenticationService)
        {
            _authentificationModel = new AuthentificationModel(authenticationService);
        }

        [HttpGet]
        public IActionResult Index()
        {
            User user = _authentificationModel.GetUserByName(User.Identity.Name);
            List<string> tags = _authentificationModel.GetTagsForUser(user.Nickname);
            var tagsString = string.Join(" ", tags);

            EditProfileViewModel model =
                new EditProfileViewModel {Email = user.Email, Nickname = user.Nickname, Tags = tagsString, Id = user.Id, OldTags = tagsString, OldNickname = user.Nickname, OldEmail = user.Email};
            
            return View("Profile", model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(EditProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                List<string> errors = await _authentificationModel.UpdateUserProfile(model);
                if (errors != null && errors.Count != 0)
                {
                    foreach (var error in errors)
                    {
                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }

            return View("Profile", model);
        }
    }
}
