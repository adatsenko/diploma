﻿using System;
using AskPeople.BLL.Interfaces;
using AskPeople.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AskPeople.Web.Models;

namespace AskPeople.Web.Controllers
{
    public class AuthentificationController : Controller
    {
        private readonly AuthentificationModel _authentificationModel;

        public AuthentificationController(IAuthenticationService authenticationService)
        {
            _authentificationModel = new AuthentificationModel(authenticationService);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View("Registration");
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {

                var errors = await _authentificationModel.ValidateAndRegister(model);
                if (errors == null || errors.Count == 0)
                {
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in errors)
                {
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            return View("Registration", model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View("Login", new LoginViewModel{ReturnUrl = returnUrl});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var errors = await _authentificationModel.SignInUser(model);
                if (errors == null || errors.Count == 0)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }

                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in errors)
                {
                    ModelState.AddModelError(String.Empty, error);
                }
            }

            return View("Login", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _authentificationModel.SignOutUser();

            return RedirectToAction("Index", "Home");
        }
    }
}
